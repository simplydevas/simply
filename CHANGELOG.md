# Changelog for Simply

## v.0.1.0 - 2024-12-30

* Validator now returns the complete rule, with attributes, as an array
* Added rule json
* Added rule lengthbetween
* Added rule valuebetween

## v.0.0.9 - 2024-07-15

* Fixed warnings in IO and Validate class.

## v.0.0.8 - 2024-07-01

* Translate: bugfix (only variables should be passed by reference blah blah..)

## v.0.0.7 - 2023-11-12

* Validator: added date:format and datetime:format as rules.

## v.0.0.6 - 2023-11-08

* Fixing some merge errors from previous version, nothing new.

## v0.0.5 - 2023-05-10

* Adjusted Database::sanitizeTableName to allow the same characters as this: https://dev.mysql.com/doc/refman/8.0/en/identifiers.html
* Added license to composer.json and updated LICENSE file

## v0.0.4 - 2023-03-31

Updated README and CHANGELOG.

## v0.0.3 - 2023-03-31

New class: Component

## v0.0.2 - 2023-03-27

Added LICENSE.

## v0.0.1 - 2023-03-26

First release :)
