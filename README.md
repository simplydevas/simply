# Simply

Documentation of this beautiful collection of code.

## Classes

* Database
* Component
* Email
* Format
* Registry
* Router
* Tools
* Translate
* Validate
* Validator

See README-files in each folder.

## Constants

Recommended if you are using the router:

    define("SIMPLY_PROJECT_PATH", "/path/to/project/root");

If not set, this is set to the same path as Simply.

Optional:

    define("SIMPLY_NUMBER_FORMAT", [ "dec" => 2, "decs" => ",", "ts" => " " ]);
    define("SIMPLY_CURRENCY_FORMAT", [ "dec" => 2, "decs" => ",", "ts" => " " ]);
    define("SIMPLY_TRANSLATION_DEFAULT_LANG", "nb_NO");