<?php

namespace Simply;

class Tools
{
    /* Debugging */

    public static function errorlog($t, $title)
    {
        error_log("------------------");
        error_log(gettype($t) . " | " . $title);
    
        if (is_object($t) || is_array($t)) {
    
            error_log(json_encode($t, JSON_PRETTY_PRINT));
            return;
        }
    
        error_log($t);
    }
    
    public static function consolelog($t)
    {
        if (php_sapi_name() === "cli") {
    
            echo ("------------------\n");
            echo (gettype($t) . "\n");
            
            if (is_object($t) || is_array($t)) {
    
                echo json_encode($t, JSON_PRETTY_PRINT) . "\n";
                return;
            }
        
            echo $t . "\n";
            
            return;
        }
    
        echo ("------------------<br>");
        echo gettype($t);
        echo "<pre>";
        print_r($t);
        echo "</pre>";
    }
    
    public static function printr($t)
    {
        consolelog($t);
    }
}
