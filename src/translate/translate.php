<?php

namespace Simply;

class Translate
{
    private static $strings = [];

    public static function load(string $lang)
    {
        if (empty($lang)) {
            errorlog("Lang not set");
            return false;
        }

        if (empty(self::$strings[$lang])) {

            self::$strings[$lang] = [];
            
            /* Load main file */

            self::loadMainFile($lang);

            /* Load subfiles */

            self::loadFolder($lang);
        }
        
        if (!is_array(self::$strings)) {
            return false;
        }

        return true;
    }

    private static function loadMainFile(string $lang)
    {
        $file = SIMPLY_PROJECT_PATH . "/lang/" . $lang . ".json";

        if (!file_exists($file)) {
            return false;
        }

        $strings = json_decode(file_get_contents($file), true);

        if (!is_array($strings)) {
            return false;
        }

        self::$strings[$lang] = $strings;
    }

    private static function loadFolder(string $lang)
    {
        $dir = SIMPLY_PROJECT_PATH . "/lang/" . $lang;

        if (!file_exists($dir)) {
            return false;
        }

        foreach (glob($dir . "/*.json") as $file) {

            $fileArray = explode("/", $file);
            $file = end($fileArray);
            $prefix = str_replace(".json", "", $file);

            if (!file_exists($dir . "/" . $file)) {
                continue;
            }

            $strings = json_decode(file_get_contents($dir . "/" . $file), true);

            if (!is_array($strings)) {
                continue;
            }

            foreach ($strings as $k => $v)
            {
                $k = $prefix . "." . $k;
                self::$strings[$lang][$k] = $v;
            }
        }
    }

    public static function all(string $lang)
    {
        self::load($lang);
        return self::$strings[$lang];
    }

    public static function translate(string $key, array $replace = NULL, string $lang = NULL)
    {
        $key = strtolower(trim($key));

        if (empty($lang) && defined("SIMPLY_TRANSLATION_DEFAULT_LANG") && !empty(SIMPLY_TRANSLATION_DEFAULT_LANG)) {
            $lang = SIMPLY_TRANSLATION_DEFAULT_LANG;
        }

        if (empty($lang)) {
            errorlog("Lang not set");
            return $key;
        }

        $strings = self::all($lang);

        if (empty($strings)) {
            return $key;
        }

        if (!array_key_exists($key, $strings)) {
            return $key;
        }

        $string = $strings[$key];

        if (is_array($replace)) {

            foreach ($replace as $k => $v)
            {
                $string = str_replace(":" . $k, $v, $string);
            }
        }

        return $string;
    }
}
