# Translate

Uses key/value in json files.

Files are places in /lang/:

    /lang/nb_NO.json
    /lang/nb_NO/buttons.json
    /lang/nb_NO/validator.json

Usage:

    _t(string $key, array $replace = NULL, string $lang = NULL)

`_t` is an alias for Translate::translate().