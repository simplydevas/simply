<?php

namespace Simply;

class Database
{
    static private $_dbh;
    static private $_transaction = false;

    public static function select(string $table, array $where = NULL, string $orderby = NULL, string $sort = NULL, bool $dryRun = false)
    {
        if (empty($table)) {

            return [
                "success" => false,
                "error" => "Database::update must have a table name"
            ];
        }

        if (!empty($sort) && !in_array(strtolower($sort), ["asc","desc"])) {
            
            return [
                "success" => false,
                "error" => "Database::select, sort must be ASC or DESC"
            ];
        }

        $data = [];
        $table = self::sanitizeTableName($table);

        $query = "SELECT * FROM `{$table}`";

        if (is_array($where)) {

            $whereQuery = [];
        
            foreach ($where as $key => $val)
            {
                $whereQuery[] = "`{$key}` = :{$key}";
                $data[$key] = $val;
            }

            $query .= " WHERE " . implode(" AND ", $whereQuery);
        }

        if (!empty($orderby) && !empty($sort)) {
            $query .= " ORDER BY `{$orderby}` " . strtoupper($sort);
        }

        return self::run($query, $data, $dryRun);
    }

    public static function insert(string $table, array $insert = NULL, bool $dryRun = false)
    {
        if (empty($table)) {

            return [
                "success" => false,
                "error" => "Database::insert must have a table name"
            ];
        }

        if (!is_array($insert)) {

            return [
                "success" => false,
                "error" => "Database::insert must have a some data to insert"
            ];
        }        

        $data = [];
        $table = self::sanitizeTableName($table);

        $insertQuery1 = [];
        $insertQuery2 = [];
        
        foreach ($insert as $key => $val)
        {
            $insertQuery1[] = "`{$key}`";
            $insertQuery2[] = ":{$key}";
            $data[$key] = $val;
        }

        $query = "INSERT INTO `{$table}` (" . implode(", ", $insertQuery1) . ") VALUES (" . implode(", ", $insertQuery2) . ")";

        return self::run($query, $data, $dryRun);
    }

    public static function update(string $table, array $update, array $where = NULL, bool $dryRun = false)
    {
        if (empty($table)) {

            return [
                "success" => false,
                "error" => "Database::update must have a table name"
            ];
        }

        if (!is_array($update)) {

            return [
                "success" => false,
                "error" => "Database::update must have a some data to update"
            ];
        }

        $data = [];
        $table = self::sanitizeTableName($table);

        $updateQuery = [];
        
        foreach ($update as $key => $val)
        {
            $updateQuery[] = "`{$key}` = :{$key}";
            $data[$key] = $val;
        }

        $query = "UPDATE `{$table}` SET " . implode(", ", $updateQuery);

        if (is_array($where)) {

            $whereQuery = [];
        
            foreach ($where as $key => $val)
            {
                $whereQuery[] = "`{$key}` = :{$key}";
                $data[$key] = $val;
            }

            $query .= " WHERE " . implode(" AND ", $whereQuery);
        }

        return self::run($query, $data, $dryRun);
    }

    public static function delete(string $table, array $where, bool $dryRun = false)
    {
        if (empty($table)) {

            return [
                "success" => false,
                "error" => "Database::delete must have a table name"
            ];
        }

        if (!is_array($where)) {

            return [
                "success" => false,
                "error" => "Database::delete must have a WHERE clause"
            ];
        }

        $data = [];
        $table = self::sanitizeTableName($table);

        $whereQuery = [];
        
        foreach ($where as $key => $val)
        {
            $whereQuery[] = "`{$key}` = :{$key}";
            $data[$key] = $val;
        }

        $query = "DELETE FROM `{$table}` WHERE " . implode(" AND ", $whereQuery);

        return self::run($query, $data, $dryRun);
    }

    public static function beginTransaction()
    {
        self::init();
        self::$_dbh->beginTransaction();
        self::$_transaction = true;

        return true;
    }

    public static function rollBack()
    {
        self::$_dbh->rollBack();
        self::$_transaction = false;

        return true;
    }

    public static function commit()
    {
        if (self::$_transaction == false) {
            return false;
        }

        self::$_dbh->commit();

        return true;
    }

    private static function init()
    {
        if (empty(self::$_dbh)) {

            if ( !defined("MYSQL_HOSTNAME") || empty(MYSQL_HOSTNAME) || !defined("MYSQL_DATABASE") || empty(MYSQL_DATABASE) || !defined("MYSQL_USERNAME") || empty(MYSQL_USERNAME) || !defined("MYSQL_PASSWORD") || empty(MYSQL_PASSWORD) ) {
                die("Simply: missing MySQL-config");
            }

            try {
                self::$_dbh = new \PDO("mysql:host=" . MYSQL_HOSTNAME . ";dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
                self::$_dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
            
            catch(\PDOException $e) {
                die("PDO: connection failed: "  . $e->getMessage());
            }
        }
    }

    public static function run(string $query, array $data = NULL, $dryRun = false)
    {
        self::init();

        if (is_object($data)) {
            $data = (array) $data;
        }

        if ($dryRun) {

            foreach ($data as $name => $value)
            {
                $query = str_replace(":{$name}", "'" . $value . "'", $query);
            }

            return [
                "success" => true,
                "count" => NULL,
                "id" => NULL,
                "result" => NULL,
                "data" => $data,
                "dryrun" => true,
                "query" => $query
            ];
        }

        $humanQuery = $query;

        try {

            $stmt = self::$_dbh->prepare($query);

            if (is_array($data)) {
    
                foreach ($data as $name => $value)
                {
                    switch (true)
                    {
                        case empty($value):
                            $type = \PDO::PARAM_NULL;
                            $value = null;
                            break;
                        case is_null($value):
                            $type = \PDO::PARAM_NULL;
                            break;
                        case is_int($value):
                            $type = \PDO::PARAM_INT;
                            break;
                        case is_bool($value):
                            $type = \PDO::PARAM_BOOL;
                            break;
                        case is_null($value):
                            $type = \PDO::PARAM_NULL;
                            break;
                        default:
                            $type = \PDO::PARAM_STR;
                    }

                    $stmt->bindValue(":{$name}", $value, $type);
                    $humanQuery = str_replace(":{$name}", "'" . $value . "'", $humanQuery);
                }
            }

            $stmt->execute();

            return [
                "success" => true,
                "count" => $stmt->rowCount(),
                "id" => (self::$_dbh->lastInsertId()) ? self::$_dbh->lastInsertId() : NULL,
                "result" => $stmt->fetchAll(\PDO::FETCH_OBJ),
                "data" => $data,
                "query" => $humanQuery
            ];
        }
        
        catch (\Exception $e) {

            error_log("PDOERROR: " . implode("|", $stmt->errorInfo()));

            // echo "<pre>";
            // $stmt->debugDumpParams();
            // echo "</pre>";

            return [
                "success" => false,
                "count" => 0,
                "errorCode" => $stmt->errorCode(),
                "errorInfo" => $stmt->errorInfo(),
                "query" => $humanQuery
            ];
        }
    }

    private static function sanitizeTableName($name)
    {
        return preg_replace("/[^0-9a-zA-Z_]/", "", $name);
    }
}
