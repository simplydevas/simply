<?php

namespace Simply;

use ArrayAccess;
use Iterator;

class Collection implements ArrayAccess, Iterator
{
    public function __construct(private $items)
    {
    }

    public static function make(array $items)
    {
        return new static($items);
    }

    public function all(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->items;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->items[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        if ($this->offsetExists($offset)) {
            return $this->items[$offset];
        }

        return null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        if ($offset == NULL) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->items[$offset]);
    }

    public function dd()
    {
        printr($this->items,true);
    }

    public function dump()
    {
        printr($this->items);
    }

    public function current(): mixed
    {
        return current($this->items);
    }

    public function key(): mixed
    {
        return key($this->items);
    }

    public function next(): void
    {
        next($this->items);
    }

    public function rewind(): void
    {
        reset($this->item);
    }

    public function valid(): bool
    {
        return isset($this->items[$this->key()]);
    }

    public function map(callable $callback): self
    {
        $keys = array_keys($this->items);

        $items = array_map($callback, $this->items, $keys);

        return new static(array_combine($keys, $items));
    }

    public function filter(?callable $callback = null): self
    {
        return new static(array_filter($this->items, $callback));
    }

    public function reduce(callable $callback, int|number|decimal $initial = null)
    {
        return array_reduce($this->items, $callback, $initial);
    }

    public function sum(?string $key = null): self
    {
        return new static(["sum" => $this->reduce(fn ($sum, $item) => $sum + ($key == null ? $item : $item[$key]), 0)]);
    }


    public function first(callable $callback = null, $default = null)
    {
        if (is_null($callback)) {
            if (empty($this->items)) {
                return new static($default);
            }

            foreach ($this->items as $item) {
                return new static($item);
            }
        }

        foreach ($this->items as $key => $value) {
            if ($callback($value, $key)) {
                return new static($value);
            }
        }

        return new static($default);
    }

    public function last(callable $callback = null, $default = null)
    {
        if (is_null($callback)) {
            return empty($this->items) ? new static($default) : new static(end($this->items));
        }

        $this->items = array_reverse($this->items, true);

        return new static($this->first($callback, $default));
    }

    public function flip()
    {
        return new static(array_flip($this->items));
    }

    public function flatten($depth = INF)
    {
        //
    }

    public function shuffle($seed = null)
    {
        if (is_null($seed)) {
            shuffle($this->items);
        } else {
            mt_srand($seed);
            shuffle($this->items);
            mt_srand();
        }

        return new static($this->items);
    }
}
