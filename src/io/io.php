<?php

namespace Simply;

class IO
{
    public static function output(int $httpCode = 200, string $contentType, mixed $data = NULL)
    {
        http_response_code($httpCode);

        if ($contentType == "json") {
            header("Content-type: application/json; charset=utf-8");

            if (!empty($data)) {
                echo json_encode($data);
            }
        }

        exit;
    }

    public static function success(string $contentType, mixed $data = NULL)
    {
        self::output(200, $contentType, $data);
    }

    public static function unauthorized(string $contentType, mixed $data = NULL)
    {
        self::output(401, $contentType, $data);
    }

    public static function forbidden(string $contentType, mixed $data = NULL)
    {
        self::output(403, $contentType, $data);
    }

    public static function notFound(string $contentType, mixed $data = NULL)
    {
        self::output(404, $contentType, $data);
    }

    public static function data(string $key = NULL)
    {
        if (isset($_GET) && is_array($_GET) && count($_GET) > 0) {

            if (!empty($key)) {
                return $_GET[$key];
            }

            return $_GET;
        }

        $body = file_get_contents('php://input');

        if (!empty($body)) {

            if (Validate::isJson($body)) {

                $result = json_decode($body, true);

                if (!empty($key) && array_key_exists($key, $result)) {
                    return $result[$key];
                }

                return $result;
            }

            parse_str($body, $result);

            if (!empty($key)) {
                return $result[$key];
            }

            return $result;
        }

        return [];
    }
}
