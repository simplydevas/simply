# IO

Handles input and output.

## Input

data(string $key = NULL) gets data from GET, POST and body. Returns array, or the value for the given key.

## Output

For most cases, use success(), unauthorized(), forbidden() and notFound().

### output($httpCode = 200, $contentType, $data = NULL)

Outputs header with given httpCode, contentType and data.

Example:

    output(200, "json", [ "foo" => "bar" ])

### output($httpCode = 200, $contentType, $data = NULL)

Outputs header with given httpCode, contentType and data.

Example:

    output(200, "json", [ "foo" => "bar" ])

### success($contentType, $data), unauthorized(), forbidden() and notFound()

Outputs header with attached httpCode, given contentType and data. Ex. notFound will use httpCode 404.