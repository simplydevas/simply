<?php

namespace Simply;

class API
{
    private static $dir = SIMPLY_PROJECT_PATH . "/endpoint";

    public static function routeLoader($route)
    {
        self::endpoint($route->file);
    }

    public static function endpoint(string $name)
    {
        $file = self::$dir . "/" . str_replace(".", "/", $name) . ".php";

        if (!file_exists($file)) {
            errorlog("Could not load API endpoint " . $file);
            return;
        }

        require_once($file);
    }
}