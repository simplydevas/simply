<?php

namespace Simply;

class Validate
{
    /* Date / DateTime */

    public static function isDateTime(string $date, string $format = null)
    {
        if (empty($format)) {
            $format = "Y-m-d H:i:s";
        }
        $dateTime = \DateTime::createFromFormat($format, $date);
        return $dateTime !== false;
    }

    public static function isDate(string $date, string $format = null)
    {
        if (empty($format)) {
            $format = "Y-m-d";
        }

        $dateTime = \DateTime::createFromFormat($format, $date);
        return $dateTime !== false;
    }

    /* Formats */

    public static function isEmail(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    /* Data type */

    public static function isJson(mixed $data)
    {
        json_decode($data);
        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function isInt(mixed $data)
    {
        if (!filter_var($data, FILTER_VALIDATE_INT)) {
            return false;
        }

        return true;
    }

    public static function isFloat(mixed $data)
    {
        if (!filter_var($data, FILTER_VALIDATE_FLOAT)) {
            return false;
        }

        return true;
    }

    public static function isNumber(mixed $data)
    {
        if (!is_numeric($data)) {
            return false;
        }

        return true;
    }

    /* String length */

    public static function maxLength(mixed $data, int $length)
    {
        if (mb_strlen($data) > $length) {
            return false;
        }

        return true;
    }

    public static function minLength(mixed $data, int $length)
    {
        if (mb_strlen($data) < $length) {
            return false;
        }

        return true;
    }

    public static function lengthBetween(mixed $data, int $minLength, int $maxLength)
    {
        if (mb_strlen($data) < $minLength || mb_strlen($data) > $maxLength) {
            return false;
        }

        return true;
    }

    /* Number value */

    public static function minValue(mixed $data, int $value)
    {
        $data = floatval($data);

        if ($data < $value) {
            return false;
        }

        return true;
    }

    public static function maxValue(mixed $data, int $value)
    {
        $data = floatval($data);

        if ($data > $value) {
            return false;
        }

        return true;
    }

    public static function valueBetween(mixed $data, int $minValue, int $maxValue)
    {
        $data = floatval($data);

        if ($data < $minValue || $data > $maxValue) {
            return false;
        }

        return true;
    }

    /* Misc */

    public static function inArray(string|array $needle, string|array $haystack)
    {
        if (empty($needle) || empty($haystack)) {
            return false;
        }

        if (is_string($haystack)) {
            $haystack = [ $haystack ];
        }

        if (is_array($needle)) {

            foreach ($needle as $n)
            {
                if (in_array($n, $haystack)) {
                    return true;
                }
            }

            return false;
        }

        return in_array($needle, $haystack);
    }
}
