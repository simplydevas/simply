# Router

Callback function will get the whole route-object.

Usage:

    Router::setMode("api"); - if used, no pages is loaded
    Router::setCallback("callbackFunction"); - defaults to API and Page-class if not set.
    Router::setMiddleware("Middleware");
    Router::setNotFound("notfound"); - defaults to "404" (view/404.php).

    Router::api("GET", "/foo", "foo");

    Router::group([ "prefix" => "/user", "middleware" => "mwtest" ], function() {
        Router::api("GET", "/", "user.list");
        Router::api("GET", "/{id|int}", "user.single");
        Router::api("POST", "/", "user.create");
        Router::api("DELETE", "/", "user.delete");
    });

    Router::run();

## Main file

You can set up the "root" file by using Router::page("/").
If this is not set up, the router will try to load the file called "main".

## Middleware

Example `Router::setMiddleware("MW");` will set the class `MW` to act as the class for all middleware.
When using `"middleware" => "mwtest"` in a group or endpoint, the router will call for `MW::mwtest($route)` and use the bool return to continue or halt.

## .htaccess to use with router

    RewriteEngine On
    RewriteBase /
    RewriteRule ^index\.php$ - [L]
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule . index.php [L]