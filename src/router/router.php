<?php

namespace Simply;

class Router
{
    /* Settings */
    private static $callback;
    private static $middleware;
    private static $pageNotFound = "404";
    private static $mode;

    /* Data */
    private static $init = false;
    private static $url;
    private static $method;
    private static $path;
    private static $route;
    private static $routes = [];

    /* Temporary */
    private static $_prefix;
    private static $_middleware;

    private static function init()
    {
        $basedir = trim(str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']), "/");
        self::$method = $_SERVER['REQUEST_METHOD'];
        self::$url = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . "/" . trim(strtok($_SERVER['REQUEST_URI'], "?" ), "/");
        self::$path = "/" . trim(str_replace($basedir, "", strtok($_SERVER['REQUEST_URI'], "?")), "/");
        self::$init = true;
    }

    public static function getMethod()
    {
        return self::$method;
    }

    public static function getPath()
    {
        return self::$path;
    }

    public static function getRoute()
    {
        return self::$route;
    }

    public static function getAttr($key = NULL)
    {
        if (empty(self::$route->attr)) {
            return;
        }

        if (!empty($key)) {

            if (array_key_exists($key, self::$route->attr)) {
                return self::$route->attr[$key];
            }

            return;
        }

        return self::$route->attr;
    }

    public static function setCallback($callback)
    {
        if (empty($callback)) {
            return;
        }

        self::$callback = $callback;
    }

    public static function setMiddleware($middleware)
    {
        if (empty($middleware)) {
            return;
        }

        self::$middleware = $middleware;
    }

    public static function setNotFound($page)
    {
        if (empty($page)) {
            return;
        }

        self::$pageNotFound = $page;
    }

    public static function setMode($mode)
    {
        if (empty($mode)) {
            return;
        }

        self::$mode = strtolower($mode);
    }

    public static function group(array $options, $cb)
    {
        self::init();

        if (!empty($options['prefix'])) {

            $prefix = trim($options['prefix']);
            $prefix = trim($prefix, "/");
            $prefix = "/" . $prefix;
            self::$_prefix = $prefix;
        }

        if (!empty($options['middleware'])) {
            self::$_middleware = trim($options['middleware']);
        }

        if (!empty($cb)) {
            call_user_func($cb);
        }

        self::$_prefix = NULL;
        self::$_middleware = NULL;
    }

    public static function api(string|array $method, string $path, string $file, array $options = NULL)
    {
        self::add("api", $method, $path, $file, $options);
    }

    public static function page(string $path, string $file, array $options = NULL)
    {
        self::add("page", ["GET","POST"], $path, $file, $options);
    }

    public static function add(string $type, string|array $method, string $path, string $file, array $options = NULL)
    {
        self::init();

        $path = trim($path);
        $path = "/" . trim($path, "/") . "/";
        $path = str_replace("//", "", $path);

        if (!empty(self::$_prefix)) {
            $path = self::$_prefix . $path;
        }

        if (!in_array($type, ["page","api"])) {
            return;
        }

        if (!Validate::inArray($method, ["POST","GET","PUT","PATCH","DELETE"])) {
            return;
        }

        if (is_array($method)) {
            $id = implode("_", $method) . "_" . trim($path);
        }

        else {
            $id = trim($method) . "_" . trim($path);
        }

        $middleware = "";

        if (!empty($options['middleware'])) {
            $middleware = $options['middleware'];
        }

        else if (!empty(self::$_middleware)) {
            $middleware = self::$_middleware;
        }

        $route = (object) [
            "type" => trim($type),
            "method" => $method,
            "path" => trim($path),
            "middleware" => $middleware,
            "file" => $file,
            "options" => $options,
            "id" => $id
        ];

        $route->parts = self::routeParts($route);
        $route = self::routePattern($route);

        self::$routes[$id] = $route;
    }

    public static function run()
    {
        if (self::$path == "/") {

            if (self::$mode == "api") {
                die(':)');
            }

            $file = "main";

            foreach (self::$routes as $route)
            {
                if ($route->path == "/" || $route->path == "") {
                    $file = $route->file;
                    break;
                }
            }

            if (empty(self::$callback)) {
                Page::routeLoader((object) [ "path" => "/", "type" => "page", "file" => $file ]);
            }

            else {
                call_user_func(self::$callback, (object) [ "path" => "/", "type" => "page", "file" => $file ]);
            }
            
            die();
        }

        foreach (self::$routes as $route)
        {
            if (!self::validateMethod($route)) {
                continue;
            }
             
            if (!self::validatePattern($route)) {
                continue;
            }

            $route->attr = self::routeAttr($route);
            self::$route = $route;

            if (!empty($route->middleware)) {
                
                if (empty(self::$middleware)) {
                    die("Middleware not set");
                }

                if (is_object($route->middleware)) {
                    $mwFunction = $route->middleware->function;
                    $mwData = $route->middleware->data;
                }

                else {
                    $mwFunction = $route->middleware;
                    $mwData = NULL;
                }

                $middlewareResult = call_user_func([self::$middleware, $mwFunction], $route, $mwData);

                if ($middlewareResult == false) {
                    call_user_func([self::$middleware, "failed"], $route, $mwData);
                }
            }

            if (empty(self::$callback)) {

                if ($route->type == "api") {
                    API::routeLoader($route);
                }

                else {
                    Page::routeLoader($route);
                }
            }

            else {
                call_user_func(self::$callback, $route);
            }

            return;
        }

        if (empty(self::$callback)) {

            if (self::$mode == "api") {
                IO::notFound("json");
            }

            else {
                Page::routeLoader((object) [ "path" => "/", "type" => "page", "file" => self::$pageNotFound ]);
            }
        }

        else {
            call_user_func(self::$callback, (object) [ "path" => "/", "type" => "page", "file" => self::$pageNotFound ]);
        }

        die();
    }

    /* Private */

    private static function routeParts($route)
    {
        $parts = explode("/", $route->path);
        $parts = array_filter($parts);

        return $parts;
    }

    private static function routePattern($route)
    {
        $pattern = [];

        $parts = explode("/", $route->path);
        $parts = array_filter($parts);

        if (!is_countable($parts)) {
            return "";
        }

        $route->parts = $parts;
        $route->attr = [];

        $i = 0;
        foreach ($parts as $part)
        {
            if (!str_starts_with($part, "{")) {

                $pattern[] = $part;
                $i++;
                continue;
            }

            else {

                $part = str_replace(["{", "}"], "", $part);
                $arr = explode("|", $part);

                if (!empty($arr[1]) && $arr[1] == "int") {
                    $pattern[] = "(\d+)";
                }

                else {
                    $pattern[] = "([\w\-]+)";
                }
            }

            $i++;
        }

        $route->pattern = "#^/" . implode("\/", $pattern) . "$#i";

        return $route;
    }

    private static function routeAttr($route)
    {
        $attr = [];

        $i = 0;
        foreach ($route->parts as $part)
        {
            if (!str_starts_with($part, "{")) {
                continue;
            }

            $part = str_replace(["{", "}"], "", $part);
            $arr = explode("|", $part);
            $attr[$i] = $arr;
            $i++;
        }

        preg_match_all($route->pattern, self::$path, $matches);

        if (is_array($matches)) {
            array_shift($matches);
        }

        if (!is_countable($matches)) {
            return $route;
        }

        $result = [];

        $i = 0;
        foreach ($matches as $m) {

            if (array_key_exists($i, $attr)) {
                
                $name = $attr[$i][0];

                if (!empty($attr[$i][1]) && $attr[$i][1] == "int") {
                    $result[$name] = (int) $m[0];
                }

                else {
                    $result[$name] = $m[0];
                }
            }

            $i++;
        }

        return $result;
    }

    private static function validateMethod($route)
    {
        if (!Validate::inArray($route->method, self::$method)) {
            return false;
        }

        return true;
    }

    private static function validatePattern($route)
    {
        if (!preg_match($route->pattern, self::$path, $matches)) {
            return false;
        }

        return true;
    }
}