# Validator

Validate dataset using methods in Validate-class.

## Usage

    $data = IO::data();

    $rules = [
        "id" => "int",
        "name" => "required",
        "email" => "email",
        "password" => "minLength:8"
    ];

    $validator = new Validator($data, $rules);

    $validator->failed();
    $validator->data();

## Methods

* nullable
* required (use this only if its the only rule - all fields are required unless nullable)
* email
* int
* float
* number
* array
* object
* minLength
* maxLength
* minValue
* maxValue