<?php

namespace Simply;

class Validator
{
    private $failedCount = 0;
    private $failed = [];
    private $data = [];

    function __construct(array $inData, array $rules)
    {       
        foreach ($rules as $key => $ruleset)
        {
            if (!array_key_exists($key, $inData)) {
                continue;
            }

            $value = $inData[$key];
            $this->data[$key] = $value;

            $rulesets = explode("|", $rules[$key]);

            if (in_array("nullable", $rulesets) && empty($value)) {
                $this->result(true, $key, $value);
                continue;
            }

            else if (empty($value)) {
                $this->result(false, $key, $value, null, "required");
                continue;
            }

            if (is_array($rulesets)) {

                foreach ($rulesets as $r)
                {
                    if ($r == "nullable" || $r == "required") {
                        continue;
                    }

                    $rule = explode(":", $r);
                    $method = "rule_" . $rule[0];

                    if (self::$method($value, $rule) == false) {
                        $this->result(false, $key, $value, $rule, $r);
                        continue;
                    }
                }
            }
        }
    }

    private function result(bool $valid, string $key, mixed $value, array $rule = null, string $error = null)
    {
        $this->data[$key] = $value;

        if ($valid == false) {
            $this->failedCount++;
            $this->failed[$key] = [
                "value" => $value,
                "error" => $error,
                "rule" => $rule
            ];
        }
    }

    public function failed()
    {
        if ($this->failedCount > 0) {
            return $this->failed;
        }
        
        return false;
    }

    public function data()
    {
        return $this->data;
    }

    /* Validation methods */

    public static function rule_json(mixed $value, array $rule)
    {
        return Validate::isJson($value);
    }

    public static function rule_email(mixed $value, array $rule)
    {
        return Validate::isEmail($value);
    }

    public static function rule_int(mixed $value, array $rule)
    {
        return Validate::isInt($value);
    }

    public static function rule_float(mixed $value, array $rule)
    {
        return Validate::isFloat($value);
    }

    public static function rule_number(mixed $value, array $rule)
    {
        return Validate::isNumber($value);
    }

    public static function rule_array(mixed $value, array $rule)
    {
        return is_array($value);
    }

    public static function rule_object(mixed $value, array $rule)
    {
        return is_object($value);
    }

    public static function rule_minLength(mixed $value, array $rule)
    {
        return Validate::minLength($value, $rule[1]);
    }

    public static function rule_maxLength(mixed $value, array $rule)
    {
        return Validate::maxLength($value, $rule[1]);
    }

    public static function rule_lengthBetween(mixed $value, array $rule)
    {
        return Validate::lengthBetween($value, $rule[1], $rule[2]);
    }

    public static function rule_minValue(mixed $value, array $rule)
    {
        return Validate::minValue($value, $rule[1]);
    }

    public static function rule_maxValue(mixed $value, array $rule)
    {
        return Validate::maxValue($value, $rule[1]);
    }

    public static function rule_valueBetween(mixed $value, array $rule)
    {
        return Validate::valueBetween($value, $rule[1], $rule[2]);
    }

    public static function rule_date(mixed $value, array $rule)
    {
        return Validate::isDate($value, $rule[1]);
    }

    public static function rule_datetime(mixed $value, array $rule)
    {
        return Validate::isDateTime($value, $rule[1]);
    }
}
