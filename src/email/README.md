# Email

Send emailz. Only sends HTML emails.

## Config

    define("SIMPLY_SMTP_HOSTNAME", "");
    define("SIMPLY_SMTP_PORT", 25);
    define("SIMPLY_SMTP_FROM_NAME", "");
    define("SIMPLY_SMTP_FROM_ADDRESS", "");

## Quick send

    Simply\Email::send("foo@bar.com", "Subject", "<p>The body</p>");

## Full object

    $email = new Simply\Email("The subject", "The body");
    $email->addAddress( string | csv | array );
    $email->addCC( string | csv | array );
    $email->addBCC( string | csv | array );
    $email->addAttachment( string | csv | array );
    $email->result();

## Return

    return [
        "success" => bool,
        "result" => PHPMailer object,
        "errorInfo" => ErrorInfo object
    ];