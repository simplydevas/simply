<?php

namespace Simply;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email
{
    private $subject;
    private $body;
    private $addresses = [];
    private $replyto = [];
    private $cc = [];
    private $bcc = [];
    private $attachments = [];
    private $errors = [];

    function __construct(string $subject, string $body)
    {
        if (empty($subject)) {
            $this->errors[] = "Missing subject";
        }

        else {
            $this->subject = $subject;
        }

        if (empty($body)) {
            $this->errors[] = "Missing body";
        }

        else {
            $this->body = $body;
        }
    }

    public function addAddress(mixed $addresses)
    {
        if (!is_array($addresses)) {
            $addresses = explode(",", $addresses);
        }

        foreach ($addresses as $address)
        {
            if (!Validate::isEmail($address)) {
                $this->errors[] = $address;
                continue;
            }

            $this->addresses[] = $address;
        }
    }

    public function addCC(mixed $addresses)
    {
        if (!is_array($addresses)) {
            $addresses = explode(",", $addresses);
        }

        foreach ($addresses as $address)
        {
            if (!Validate::isEmail($address)) {
                $this->errors[] = $address;
                continue;
            }

            $this->cc[] = $address;
        }
    }

    public function addBCC(mixed $addresses)
    {
        if (!is_array($addresses)) {
            $addresses = explode(",", $addresses);
        }

        foreach ($addresses as $address)
        {
            if (!Validate::isEmail($address)) {
                $this->errors[] = $address;
                continue;
            }

            $this->bcc[] = $address;
        }
    }

    public function addAttachment(mixed $attachments)
    {
        if (!is_array($attachments)) {
            $attachments = explode(",", $attachments);
        }

        foreach ($attachments as $attachment)
        {
            $this->attachments[] = $attachment;
        }
    }

    public function result()
    {
        if (!defined("SIMPLY_SMTP_FROM_NAME")) {
            die("Missing: SIMPLY_SMTP_FROM_NAME");
        }

        if (!defined("SIMPLY_SMTP_FROM_ADDRESS")) {
            die("Missing: SIMPLY_SMTP_FROM_ADDRESS");
        }

        if (!defined("SIMPLY_SMTP_HOSTNAME")) {
            die("Missing: SIMPLY_SMTP_HOSTNAME");
        }

        if (!defined("SIMPLY_SMTP_PORT")) {
            die("Missing: SIMPLY_SMTP_PORT");
        }

        if (count($this->errors) > 0) {

            return [
                "success" => false,
                "errors" => $this->errors
            ];
        }

        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->SMTPAuth = false;
            $mail->Host = SIMPLY_SMTP_HOSTNAME;
            $mail->Port = SIMPLY_SMTP_PORT;
            $mail->setFrom(SIMPLY_SMTP_FROM_ADDRESS, SIMPLY_SMTP_FROM_NAME);
            $mail->isHTML(true);
            $mail->Subject = $this->subject;
            $mail->Body = $this->body;

            if (count($this->addresses)) {

                foreach ($this->addresses as $address)
                {
                    $mail->addAddress(trim($address));
                }
            }

            if (count($this->replyto)) {

                foreach ($this->replyto as $address)
                {
                    $mail->addReplyTo(trim($address));
                }
            }

            if (count($this->cc)) {

                foreach ($this->cc as $address)
                {
                    $mail->addCC(trim($address));
                }
            }

            if (count($this->bcc)) {

                foreach ($this->bcc as $address)
                {
                    $mail->addBCC(trim($address));
                }
            }

            if (count($this->attachments)) {

                foreach ($this->attachments as $attachment)
                {
                    $mail->addAttachment($attachment);
                }
            }

            $mail->send();

            return [
                "success" => true,
                "phpmailer" => $mail
            ];

        } catch (Exception $e) {

            return [
                "success" => false,
                "phpmailer" => $mail,
                "error" => $mail->ErrorInfo
            ];
        }
    }

    public static function send(string $to, string $subject, string $body)
    {
        $mail = new self($subject, $body);
        $mail->addAddress($to);
        return $mail->result();
    }
}
