<?php

namespace Simply;

class Registry
{
    static private $_registry_storage = array();

    public static function set($key, $data = NULL)
    {
        if (empty($key)) {
            return false;
        }

        $key = self::sanitizeKey($key);

        self::$_registry_storage[$key] = $data;

        return true;
    }

    public static function has($key)
    {
        if (empty($key)) {
            return false;
        }

        $key = self::sanitizeKey($key);

        if (!is_array(self::$_registry_storage) || !array_key_exists($key, self::$_registry_storage)) {
            return false;
        }

        return true;
    }

    public static function get($key)
    {
        if (empty($key)) {
            return false;
        }

        $key = self::sanitizeKey($key);

        if (!is_array(self::$_registry_storage) || !array_key_exists($key, self::$_registry_storage)) {
            return false;
        }

        return self::$_registry_storage[$key];
    }

    public static function delete($key)
    {
        if (empty($key)) {
            return false;
        }

        $key = self::sanitizeKey($key);

        unset(self::$_registry_storage[$key]);
    }

    public static function all()
    {
        return self::$_registry_storage;
    }
    
    private static function sanitizeKey($key)
    {
        $key = strtolower($key);
        $key = preg_replace("/[^a-zA-Z._-]/", "", $key);
        return $key;
    }
}