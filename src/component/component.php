<?php

namespace Simply;

class Component
{
    private static $dir = SIMPLY_PROJECT_PATH . "/view/component";

    public static function load(string $_componentName, ...$attr)
    {
        $file = self::$dir . "/" . str_replace(".", "/", $_componentName) . ".php";

        if (!file_exists($file)) {
            echo "<span>comp-{$_componentName}</span>";
            return;
        }

        if (!empty($attr[0])) {

            foreach ($attr[0] as $k => $v)
            {
                ${$k} = $v;
            }

            unset($attr);
        }

        require($file);
    }
}
