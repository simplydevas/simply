# Component

Create file:

    view/component/foobar.php
    view/component/foo/bar.php

Use in page:

    <?php _x("foobar"); ?>
    <?php _x("foo.bar", name: $name, email: "foo@bar.com"); ?>