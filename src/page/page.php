<?php

namespace Simply;

class Page
{
    private static $pageDir = SIMPLY_PROJECT_PATH . "/view/page";
    private static $templateDir = SIMPLY_PROJECT_PATH . "/view/template";

    public static function routeLoader($route)
    {
        self::page($route->file);
    }

    public static function header()
    {
        $file = self::$pageDir . "/header.php";

        if (!file_exists($file)) {
            die("Could not load header.php");
        }

        require_once($file);
    }

    public static function footer()
    {
        $file = self::$pageDir . "/footer.php";

        if (!file_exists($file)) {
            die("Could not load footer.php");
        }

        require_once($file);
    }

    public static function page(string $name)
    {
        $file = self::$pageDir . "/" . str_replace(".", "/", $name) . ".php";

        if (!file_exists($file)) {
            die("Could not load page " . $file);
        }

        require_once($file);

        self::footer();
    }

    public static function template(string $name, mixed $data = NULL)
    {
        $file = self::$templateDir . "/" . str_replace(".", "/", $name) . ".php";

        if (!file_exists($file)) {
            die("Could not load template " . $file);
        }

        include($file);
    }
}