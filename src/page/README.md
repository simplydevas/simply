# Page

Used internally for the router to view pages.

## Functions

* header() - loads header.php. Use this in every page template.
* template() - loads a given template file.
* page() - loads a specific page. This is loaded automatically in the router.
* footer() - loads footer.php. This is loaded automatically in the router.