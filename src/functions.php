<?php

/* Debugging */

use Simply\Component;
use Simply\Translate;
use Simply\Tools;

if (!function_exists("errorlog")) {

    function errorlog($t, $title = NULL)
    {

        return Tools::errorlog($t, $title);
    }
}

if (!function_exists("consolelog")) {

    function consolelog($t)
    {
        return Tools::consolelog($t);
    }
}

if (!function_exists("printr")) {

    function printr($t, $die = NULL)
    {
        if (!function_exists('dump')) {
            return Tools::consolelog($t);
        }
        
        else {
            dump($t);
        }

        if ($die) {
            die();
        }
    }
}

if (!function_exists("_t")) {

    function _t(string $key, array $replace = NULL, string $lang = NULL)
    {
        return Translate::translate($key, $replace, $lang);
    }
}

if (!function_exists("_x")) {

    function _x(string $_componentName, ...$attr)
    {
        Component::load($_componentName, $attr);
    }
}
