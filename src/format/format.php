<?php

namespace Simply;

class Format
{
    public static function date(string $date = NULL, string $format = "Y-m-d")
    {
        $dt = new \DateTime($date);
        $result = $dt->format($format);
        return $result;
    }

    public static function dateTime(string $datetime = NULL, string $format = "Y-m-d H:i:s")
    {
        $dt = new \DateTime($datetime);
        $result = $dt->format($format);
        return $result;
    }

    public static function number(int|string|float $number, int $dec = NULL, string $decs = NULL, $ts = NULL)
    {
        if (defined("SIMPLY_NUMBER_FORMAT")) {
            
            $defaultFormat = SIMPLY_NUMBER_FORMAT;

            if (array_key_exists("dec", $defaultFormat)) {
                $dec = (int) $defaultFormat['dec'];
            }

            if (array_key_exists("decs", $defaultFormat)) {
                $decs = $defaultFormat['decs'];
            }

            if (array_key_exists("ts", $defaultFormat)) {
                $ts = $defaultFormat['ts'];
            }
        }

        return number_format($number, $dec, $decs, $ts);
    }

        public static function currency(int|string|float $number, int $dec = NULL, string $decs = NULL, $ts = NULL)
    {
        if (defined("SIMPLY_CURRENCY_FORMAT")) {
            
            $defaultFormat = SIMPLY_CURRENCY_FORMAT;

            if (array_key_exists("dec", $defaultFormat)) {
                $dec = (int) $defaultFormat['dec'];
            }

            if (array_key_exists("decs", $defaultFormat)) {
                $decs = $defaultFormat['decs'];
            }

            if (array_key_exists("ts", $defaultFormat)) {
                $ts = $defaultFormat['ts'];
            }
        }

        return number_format($number, $dec, $decs, $ts);
    }
}