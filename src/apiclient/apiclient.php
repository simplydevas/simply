<?php

namespace Simply;

class APIClient
{
    private $curl;
    private $code;
    private $raw;
    private $header;

    function __construct(string $method, string $endpoint, mixed $data = NULL, array $headers = NULL)
    {
        if (!defined("SIMPLY_API_URL")) {
            die("SIMPLY_API_URL not defined");
        }

        $this->curl = curl_init();

        if (empty($headers)) {
            $headers = [];
        }

        $headers[] = "Content-Type: application/json";

        $url = rtrim(SIMPLY_API_URL, "/") . $endpoint;

        if ($method == "POST") {

            curl_setopt($this->curl, CURLOPT_POST, 1);

            if ($data) {
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }

        else if ($method == "DELETE") {
    
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    
            if ($data) {
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }

        else if ($method == "PUT") {
    
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
    
            if ($data) {
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }
    
        else if ($method == "PATCH") {
    
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    
            if ($data) {
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }

        else if ($data) {
            $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_HEADER, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

        if (is_array($headers)) {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        }

        $output = curl_exec($this->curl);

        $this->code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $headerLength = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
        $this->header = substr($output, 0, $headerLength);
        $this->raw = substr($output, $headerLength);
    }

    public function code()
    {
        return $this->code;
    }

    public function raw()
    {
        return $this->raw;
    }

    public function data()
    {
        return json_decode($this->raw);
    }

    public function headers()
    {
        $headers = [];

        $rows = explode("\n", $this->header);

        foreach ($rows as $row)
        {
            $cols = explode(":", $row);
            $k = trim($cols[0]);
            $v = trim($cols[1]);
            $headers[$k] = $v;
        }

        return $headers;
    }
}
