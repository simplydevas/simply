<?php

define("SIMPLY_PATH", dirname(__FILE__));

if (!defined("SIMPLY_PROJECT_PATH")) {
    define("SIMPLY_PROJECT_PATH", SIMPLY_PATH);
}

spl_autoload_register(function ($class) {

    if (!str_starts_with($class, "Simply")) {
        return;
    }

    $class = str_replace("Simply\\", "", $class);
    $file = SIMPLY_PATH . "/src/" . strtolower($class) . "/" . strtolower($class) . ".php";

    require_once($file);
});

require_once(SIMPLY_PATH . "/src/functions.php");
